//
//  GoogleMusicAppDelegate.h
//  GoogleMusic
//
//  Created by Carlos on 23/08/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>


@interface GoogleMusicAppDelegate : NSObject<NSUserNotificationCenterDelegate> {
  NSWindow *window;
  WebView *webView;
  NSMenu *dockMenu;
  
@private
  NSString *_title;
  NSString *_artist;
}

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet WebView *webView;

- (IBAction)nextTrack: (id)sender;
- (IBAction)previousTrack: (id)sender;
- (IBAction)playPauseTrack: (id)sender;

- (void) toggleNext;
@end
