//
//  GoogleMusicAppDelegate.m
//  GoogleMusic
//
//  Created by Carlos on 23/08/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <WebKit/WebView.h>
#import <WebKit/WebFrame.h>
#import "GoogleMusicAppDelegate.h"

@interface WebView (WebKitHackaton)
-(void)setDrawsBackground:(BOOL)b;
@end

@implementation GoogleMusicAppDelegate

@synthesize window;
@synthesize webView;


- (void) toggleNext {
  NSLog (@"next");
}

// -----------------------------------------------------------------------------
// windowShouldClose:
// -----------------------------------------------------------------------------
- (BOOL) windowShouldClose: (id)sender 
{
  [NSApp hide: self];
  return NO;
}

// -----------------------------------------------------------------------------
// awakeFromNib
// -----------------------------------------------------------------------------
- (void)awakeFromNib
{
  [webView setDrawsBackground: NO];
  
  NSURL *url = [NSURL 
    URLWithString: @"http://music.google.com/music/listen?hl=en#start_pl"
  ];
  NSURLRequest *urlRequest = [NSURLRequest requestWithURL: url];
  
  [[webView mainFrame] loadRequest: urlRequest];
  
  [NSApp setDelegate: self];
  
  [[NSUserNotificationCenter defaultUserNotificationCenter] setDelegate: self];
}

// -----------------------------------------------------------------------------
// userNotificationCenter
// -----------------------------------------------------------------------------
- (BOOL)userNotificationCenter: (NSUserNotificationCenter *)center
     shouldPresentNotification: (NSUserNotification *)notification
{
  return YES;
}


// -----------------------------------------------------------------------------
// playPauseTrack
// -----------------------------------------------------------------------------
- (IBAction)playPauseTrack: (id)sender 
{
  [webView stringByEvaluatingJavaScriptFromString: @"$('button[data-id=\"play-pause\"]').click()"];
}

// -----------------------------------------------------------------------------
// nextTrack
// -----------------------------------------------------------------------------
- (IBAction)nextTrack: (id)sender {
  [webView stringByEvaluatingJavaScriptFromString: @"$('button[data-id=\"forward\"]').click()"];
}

// -----------------------------------------------------------------------------
// previousTrack
// -----------------------------------------------------------------------------
- (IBAction)previousTrack: (id)sender {
  [webView stringByEvaluatingJavaScriptFromString:@"$('button[data-id=\"rewind\"]').click()"];
}
   
// -----------------------------------------------------------------------------
// validateMenuItem
// -----------------------------------------------------------------------------
- (BOOL)validateMenuItem: (NSMenuItem *)menuItem {
  NSString *ret;
  switch ([menuItem tag]) {
    case 0: // Play
      ret = [webView stringByEvaluatingJavaScriptFromString: @"$('button[data-id=\"play-pause\"]').is (':disabled')"];
      break;
    case 1: // Next
      ret = [webView stringByEvaluatingJavaScriptFromString: @"$('button[data-id=\"forward\"]').is (':disabled')"];
      break;
    case 2: // Prev
      ret = [webView stringByEvaluatingJavaScriptFromString: @"$('button[data-id=\"rewind\"]').is (':disabled')"];
      break;
  }
  
  return ([ret compare: @"true"] == NSOrderedSame);
}

#define LOAD_JQUERY_DYNAMICALLY "(function(){" \
"  var newscript = document.createElement('script');" \
"  newscript.type = 'text/javascript';" \
"  newscript.async = true;" \
"  newscript.src = '//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js';" \
"  (document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(newscript);" \
"})();"

//------------------------------------------------------------------------------
// webView:didFinishLoadForFrame
//------------------------------------------------------------------------------
- (void) webView:(WebView *)sender didFinishLoadForFrame:(WebFrame *)frame
{
  [webView stringByEvaluatingJavaScriptFromString: @LOAD_JQUERY_DYNAMICALLY];
  
  
  if (frame == [sender mainFrame]) {
    // Hide some elemets  
    [webView stringByEvaluatingJavaScriptFromString: @"document.getElementsByClassName ('gb_ga')[0].remove()"];
    
    NSUserNotification *notification = [[NSUserNotification alloc] init];
    notification.title = @"Google Music is ready!";
    [[NSUserNotificationCenter defaultUserNotificationCenter] deliverNotification:notification];
  }
}

//------------------------------------------------------------------------------
// webView:didReceiveTitle
//------------------------------------------------------------------------------
- (void)  webView: (WebView *)sender
  didReceiveTitle: (NSString *)frameTitle
         forFrame: (WebFrame *)frame
{
  
  // Only report feedback for the main frame.
  if (frame == [sender mainFrame]) {
    NSLog (@"webView:didReceiveTitle:forFrame (%@)", frameTitle);
    
    NSString *ret = [webView
      stringByEvaluatingJavaScriptFromString: @"$('button[data-id=\"play-pause\"]').hasClass ('playing')"
    ];
    
    if ([ret compare: @"true"] == NSOrderedSame) {
      // Playing
      /*
      NSString *title = [webView
        stringByEvaluatingJavaScriptFromString: @"$('#playerSongTitle').text()"
      ];
      NSString *artist = [webView
        stringByEvaluatingJavaScriptFromString: @"$('#player-artist').text()"
      ];
      NSString *album = [webView
        stringByEvaluatingJavaScriptFromString: @"$('.player-album').text()"
      ];
      
      NSUserNotification *notification = [[NSUserNotification alloc] init];
      notification.title = title;
      notification.subtitle = [NSString stringWithFormat: @"%@ - %@", artist, album];
      [[NSUserNotificationCenter defaultUserNotificationCenter] deliverNotification:notification];
      */
      
      /*
      NSString *title = [webView
        stringByEvaluatingJavaScriptFromString: @"document.title"
      ];
      NSArray *components = [title componentsSeparatedByString: @" - "];
      
      if ([components count] == 3) {
        NSUserNotification *notification = [[NSUserNotification alloc] init];
        notification.title = (NSString *)[components objectAtIndex: 0];
        notification.subtitle = (NSString *)[components objectAtIndex: 1];
        [[NSUserNotificationCenter defaultUserNotificationCenter] deliverNotification: notification];
      }
       */
    }
  }
}


// ----------------------------------------------------------------------------
// webView:runOpenPanelForFileButtonWithResultListener:
// ----------------------------------------------------------------------------
- (void)                              webView: (WebView *)sender
  runOpenPanelForFileButtonWithResultListener: (id <WebOpenPanelResultListener>)resultListener
{
  NSOpenPanel* openDlg = [NSOpenPanel openPanel];
  
  [openDlg setCanChooseFiles: YES];
  
  [openDlg setCanChooseDirectories: NO];
  
  if ([openDlg runModal] == NSOKButton) {
    NSArray* URLs = [openDlg URLs];
    NSMutableArray *files = [[NSMutableArray alloc] init];
    
    for (size_t i = 0; i < [URLs count]; i++) {
      NSString *filename = [[URLs objectAtIndex: i] relativePath];
      [files addObject: filename];
    }
    
    for (size_t i = 0; i < [files count]; i++) {
      NSString* fileName = [files objectAtIndex: i];
      [resultListener chooseFilename: fileName];
    }
    
    [files release];
  }
}


@end
