// File from http://rogueamoeba.com/utm/2007/09/29/

#import <IOKit/hidsystem/ev_keymap.h>
#import "MediaKeysApplication.h"



@implementation MediaKeysApplication

@synthesize delegate;

// ----------------------------------------------------------------------------
// setDelegate
// ----------------------------------------------------------------------------
- (void) setDelegate: (id)object {
  delegate = object;
}

// ----------------------------------------------------------------------------
// mediaKeyEvent:state:repeat:
// ----------------------------------------------------------------------------
- (void) mediaKeyEvent: (int)key state: (BOOL)state repeat: (BOOL)repeat
{
	switch (key){
		case NX_KEYTYPE_PLAY:
			if (state == 0)
				// Play pressed and released
        // TODO: use SPMediaKeyTab (https://github.com/nevyn/SPMediaKeyTap) to
        // disable iTunes.
      break;
      
		case NX_KEYTYPE_FAST:
			if (state == 0) {
        // Next pressed and released
        if ([delegate respondsToSelector: @selector(nextTrack:)])
          [delegate nextTrack: self];
      }
      break;
      
		case NX_KEYTYPE_REWIND:
			if( state == 0 ) {
        // Previous pressed and released
        if ([delegate respondsToSelector: @selector(previousTrack:)])
          [delegate previousTrack: self];
      }
      break;
	}
}

// ----------------------------------------------------------------------------
// sendEvent:
// ----------------------------------------------------------------------------
- (void)sendEvent: (NSEvent*)event
{
	if( [event type] == NSSystemDefined && [event subtype] == 8 )
	{
		int keyCode = (([event data1] & 0xFFFF0000) >> 16);
		int keyFlags = ([event data1] & 0x0000FFFF);
		int keyState = (((keyFlags & 0xFF00) >> 8)) ==0xA;
		int keyRepeat = (keyFlags & 0x1);
		
		[self mediaKeyEvent: keyCode state: keyState repeat: keyRepeat];
	}
  
	[super sendEvent: event];
}
@end

