// File from http://rogueamoeba.com/utm/2007/09/29/


@interface MediaKeysApplication : NSApplication
{
  id delegate;
}

@property (nonatomic, assign, readwrite) id delegate;

- (void) setDelegate: (id)object;

@end

